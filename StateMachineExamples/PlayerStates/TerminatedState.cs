﻿using Gvr.StateMachine;

namespace StateMachineExamples.PlayerStates
{
	internal class TerminatedState : StateBase<IPlayerCommands>, IPlayerCommands
	{
		public static readonly Event Begin = new Event(PlayerEvent.Begin.ToString());
		public static readonly Event End = new Event(PlayerEvent.End.ToString());
		public static readonly Event Exit = new Event(PlayerEvent.Exit.ToString());
		public static readonly Event PauseCmd = new Event(PlayerEvent.Pause.ToString());
		public static readonly Event Resume = new Event(PlayerEvent.Resume.ToString());

		public TerminatedState(IPlayerCommands fsm, IEventSink eventSink)
			: base("TerminatedState", fsm, eventSink)
		{
		}

		public void Play()
		{
			// do nothing
		}

		public void Stop()
		{
			// do nothing
		}

		public void Pause()
		{
			// do nothing
		}
	}
}
