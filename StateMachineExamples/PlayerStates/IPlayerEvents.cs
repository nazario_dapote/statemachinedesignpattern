﻿
namespace StateMachineExamples.PlayerStates
{
	internal enum PlayerEvent
	{
		Begin,
		End,
		Pause,
		Resume,
		Exit
	}
}
