﻿using Gvr.StateMachine;

namespace StateMachineExamples.PlayerStates
{
	internal class InactiveState : StateBase<IPlayerCommands>, IPlayerCommands
	{
		public static readonly Event Begin = new Event(PlayerEvent.Begin.ToString());
		public static readonly Event End = new Event(PlayerEvent.End.ToString());
		public static readonly Event Exit = new Event(PlayerEvent.Exit.ToString());
		public static readonly Event PauseCmd = new Event(PlayerEvent.Pause.ToString());
		public static readonly Event Resume = new Event(PlayerEvent.Resume.ToString());

		public InactiveState(IPlayerCommands fsm, IEventSink eventSink)
			: base("InactiveState", fsm, eventSink)
		{
		}

		public void Play()
		{
			castEvent(Begin);
		}

		public void Stop()
		{
			castEvent(Exit);
		}

		public void Pause()
		{
		}
	}
}
