﻿namespace StateMachineExamples.PlayerStates
{
	interface IPlayerCommands
	{
		void Play();
		void Stop();
		void Pause();
	}
}
