﻿using Gvr.StateMachine;

namespace StateMachineExamples.PlayerStates
{
	internal class ActiveState : StateBase<IPlayerCommands>, IPlayerCommands
	{
		public static readonly Event Begin = new Event(PlayerEvent.Begin.ToString());
		public static readonly Event End = new Event(PlayerEvent.End.ToString());
		public static readonly Event Exit = new Event(PlayerEvent.Exit.ToString());
		public static readonly Event PauseCmd = new Event(PlayerEvent.Pause.ToString());
		public static readonly Event Resume = new Event(PlayerEvent.Resume.ToString());

		public ActiveState(IPlayerCommands fsm, IEventSink eventSink)
			: base("ActiveState", fsm, eventSink)
		{
		}

		public void Play()
		{
		}

		public void Stop()
		{
			castEvent(End);
		}

		public void Pause()
		{
			castEvent(PauseCmd);
		}
	}
}
