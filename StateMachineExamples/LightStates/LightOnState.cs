﻿using System;
using Gvr.StateMachine;

namespace StateMachineExamples.LightStates
{
	internal class LightOnState : StateBase<ILights>, ILights, IState
	{
		public readonly static Event SWITCH = new Event(LightsEvent.Switch.ToString());

		public LightOnState(ILights fsm, IEventSink eventSink)
			: base("LightOnState", fsm, eventSink)
		{
		}

		public void toggle()
		{
			castEvent(LightOnState.SWITCH);
		}

		public void onEntry()
		{
			Console.WriteLine("Light is on");
		}

		public void onExit()
		{
			// do nothing
		}
	}
}