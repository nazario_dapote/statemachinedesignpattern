﻿namespace StateMachineExamples.LightStates
{
	internal interface ILights
	{
		void toggle();
	}
}