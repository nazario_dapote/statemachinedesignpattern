﻿using System;
using Gvr.StateMachine;

namespace StateMachineExamples.LightStates
{
	internal class LightOffState : StateBase<ILights>, ILights, IState
	{
		public readonly static Event SWITCH = new Event(LightsEvent.Switch.ToString());

		public LightOffState(ILights fsm, IEventSink eventSink)
			: base("LightOffState", fsm, eventSink)
		{
		}

		public void toggle()
		{
			castEvent(LightOffState.SWITCH);
		}

		public void onEntry()
		{
			Console.WriteLine("Light is off");
		}

		public void onExit()
		{
			// do nothing
		}
	}
}