﻿namespace StateMachineExamples
{
	/// <summary>
	/// Automata Interface.
	/// Note that automata interface in the proposed pattern is implemented by the context and by the state classes.
	/// </summary>
	public interface IConnection
	{
		void connect();

		void disconnect();

		int receive();

		void send(int value);
	}
}