﻿using System;
using System.Collections.Generic;
using System.Threading;
using Gvr.StateMachine;
using Gvr.StateMachine.Extensions;
using StateMachineExamples.MultiStates;

namespace StateMachineExamples
{
	internal class Program
	{
		///////////////////////////////////////////////////////////////////////
		// Main
		///////////////////////////////////////////////////////////////////////

		private static void Main(string[] args)
		{
			ConsoleKeyInfo cki;
			do
			{
				Console.Clear();
				Console.WriteLine("Choose example:\n");
				Console.WriteLine("1. Socket Connection");
				Console.WriteLine("2. Turn the lights");
				Console.WriteLine("3. Multi states");
				Console.WriteLine("4. Player");
				Console.WriteLine("5. LoopingThread");
				Console.WriteLine("6. Delayed keyboard");
				Console.WriteLine("7. Delayed keyboard (key priority)");

				cki = Console.ReadKey(true);
				if (Char.IsNumber(cki.KeyChar))
				{
					int number;
					if (Int32.TryParse(cki.KeyChar.ToString(), out number))
					{
						Console.Clear();

						switch (number)
						{
							case 1:
								Example1(args);
								break;

							case 2:
								Example2(args);
								break;

							case 3:
								Example3(args);
								break;

							case 4:
								Example4(args);
								break;

							case 5:
								Example5(args);
								break;

							case 6:
								Example6(args);
								break;

							case 7:
								Example7(args);
								break;
						}
					}
				}
			} while (cki.Key != ConsoleKey.Escape);

			Console.WriteLine("Press any key to exit ...");
			Console.ReadKey();
		}

		///////////////////////////////////////////////////////////////////////
		// Examples
		///////////////////////////////////////////////////////////////////////

		private static void Example1(string[] args)
		{
			using (var fsm = ConnectionFsm.createAutomaton())
			{
				do
				{
					while (!Console.KeyAvailable && !fsm.isConnected)
					{
						fsm.waitConnected();
					}
				} while (Console.ReadKey(true).Key != ConsoleKey.Escape);
			}
		}

		private static void Example2(string[] args)
		{
			var fsm = new LightsFsm();

			Console.WriteLine("Press <S> to switch lights, <ESC> to exit");
			ConsoleKey key = ConsoleKey.NoName;
			do
			{
				key = Console.ReadKey(true).Key;
				if (key == ConsoleKey.S)
					fsm.toggle();
			} while (key != ConsoleKey.Escape);
		}

		private static void Example3(string[] args)
		{
			Console.WriteLine("Hint: press <S> to execute action in the state S*, <ESC> to exit");
			Console.WriteLine("Press any key to start ...");
			Console.ReadKey(true);

			bool stop = false;
			var eventsMap = new Dictionary<int, Event>()
			{
				{0, S0.Signal1},
				{1, S0.Signal2},
				{2, S1.Signal1},
				{3, S1.Signal2},
				{4, S2.Signal1},
				{5, S2.Signal2},
			};
			var r = new Random();
			int range = eventsMap.Count - 1;

			using (var mfsm = new MultiFsm())
			{
				const int TIMER = 300;

				using (
					// First timer
					var th1 = Commons.TimerCallback(() => {
						// signal random events
						mfsm.castEvent(eventsMap[r.Next(0, range)]);
					}, TIMER, () => (stop) ? Timeout.Infinite : TIMER))
				using (
					// Second timer
					var th2 = Commons.TimerCallback(() =>
					{
						// signal random events
						mfsm.castEvent(eventsMap[r.Next(0, range)]);
					}, TIMER, () => (stop) ? Timeout.Infinite : TIMER))
				{
					// Start reading keys
					ConsoleKey key = ConsoleKey.NoName;
					do
					{
						key = Console.ReadKey(true).Key;
						if (key == ConsoleKey.S)
							mfsm.Do(); // execute IMultiCmd method

					} while (Console.ReadKey(true).Key != ConsoleKey.Escape);

					stop = true;
				}
			}

		}

		private static void Example4(string[] args)
		{
			Console.WriteLine("Commands: <G> play, <S> stop, <P> pause, <ESC> exit");
			ConsoleKey key = ConsoleKey.NoName;
			bool stop = false;

			var fsm = new PlayerFsm();
			fsm.Terminated += (o, e) => {
				Console.WriteLine("StateMachine terminated.\nPress any key to exit ...");
				Console.ReadKey(true);
				stop = true;
			};

			do
			{
				key = Console.ReadKey(true).Key;
				if (key == ConsoleKey.G)
					fsm.Play();
				else if (key == ConsoleKey.S)
					fsm.Stop();
				else if (key == ConsoleKey.P)
					fsm.Pause();

			} while (key != ConsoleKey.Escape && !stop);
		}

		private static void Example5(string[] args)
		{
			Console.WriteLine("Commands: <G> start, <P> pause, <S> stop, <R> resume, <ESC> exit");
			int index = 0;
			var th = new LoopingThread(ThreadPriority.BelowNormal, () => Console.WriteLine(index++));
			th.PauseBetween = 200;

			ConsoleKey key = ConsoleKey.NoName;
			do
			{
				key = Console.ReadKey(true).Key;
				if (key == ConsoleKey.P)
					th.Pause();
				else if (key == ConsoleKey.R)
					th.Resume();
				else if (key == ConsoleKey.S)
				{
					if(th.Stop())
						Console.WriteLine("Thread stopped! No more reusable!");
				}
				else if (key == ConsoleKey.G)
				{
					if(th.Start())
						Console.WriteLine("Thread started!");
					else
						Console.WriteLine("Thread cannot be started!");
				}

			} while (key != ConsoleKey.Escape);
		}

		private static void Example6(string[] args)
		{
			Console.WriteLine("Delayed keyboard example.\nPress <F1> pause, <F2> resume, <ESC> to exit ...");
			const string PAUSE = "[Pause]";

			using (var wq = new MyWorkQueue())
			{
				ConsoleKeyInfo key;
				do
				{
					key = Console.ReadKey(true);
					if (key.Key == ConsoleKey.F1)
					{
						wq.Pause();
						DisplayStatus(PAUSE);
					}
					else if (key.Key == ConsoleKey.F2)
					{
						wq.Resume();
						ClearStatus(PAUSE);
					}
					else
						wq.AddWork(key);

				} while (key.Key != ConsoleKey.Escape);
			}
		}

		private static void Example7(string[] args)
		{
			Console.WriteLine("Delayed keyboard (with priority) example.\nPress <F1> pause, <F2> resume, <ESC> to exit ...");
			const string PAUSE = "[Pause]";

			using (var wq = new MyWorkPriorityQueue())
			{
				ConsoleKeyInfo key;
				do
				{
					key = Console.ReadKey(true);
					if (key.Key == ConsoleKey.F1)
					{
						wq.Pause();
						DisplayStatus(PAUSE);
					}
					else if (key.Key == ConsoleKey.F2)
					{
						wq.Resume();
						ClearStatus(PAUSE);
					}
					else
						wq.AddWork(key, key.Key); // priority is the key code

				} while (key.Key != ConsoleKey.Escape);
			}
		}

		///////////////////////////////////////////////////////////////////////
		// Helpers
		///////////////////////////////////////////////////////////////////////

		private class MyWorkQueue : WorkQueue<ConsoleKeyInfo>
		{
			private const int KEYBOARD_DELAY = 500;

			public MyWorkQueue()
				: base()
			{
				Start();
			}

			public override bool Start()
			{
				Console.WriteLine("Starting working queue");
				return base.Start();
			}

			public override bool Stop()
			{
				var r = base.Stop();
				Console.WriteLine("\nWorking queue terminated!");
				DisplayCountDown(TimeSpan.TicksPerSecond * 2);
				return r;
			}

			protected override void doWork(ConsoleKeyInfo item)
			{
				if (item.Key != ConsoleKey.NoName)
				{
					Thread.Sleep(KEYBOARD_DELAY);
					if (item.Key == ConsoleKey.Enter || item.Key == ConsoleKey.Escape)
						Console.WriteLine();
					else
						Console.Write(item.KeyChar);
				}
			}
		}

		private class MyWorkPriorityQueue : WorkPriorityQueue<ConsoleKeyInfo>
		{
			private const int KEYBOARD_DELAY = 500;

			public MyWorkPriorityQueue()
				: base()
			{
				Start();
			}

			public override bool Start()
			{
				Console.WriteLine("Starting working queue");
				return base.Start();
			}

			public override bool Stop()
			{
				var r = base.Stop();
				Console.WriteLine("\nWorking queue terminated!");
				DisplayCountDown(TimeSpan.TicksPerSecond * 2);
				return r;
			}

			protected override void doWork(ConsoleKeyInfo item)
			{
				if (item.Key != ConsoleKey.NoName)
				{
					Thread.Sleep(KEYBOARD_DELAY);
					if (item.Key == ConsoleKey.Enter || item.Key == ConsoleKey.Escape)
						Console.WriteLine();
					else
						Console.Write(item.KeyChar);
				}
			}
		}

		private static void DisplayStatus(string status)
		{
			if (string.IsNullOrEmpty(status))
				return;

			int l = Console.CursorLeft;
			int t = Console.CursorTop;

			var bkColor = Console.ForegroundColor;
			try
			{
				Console.ForegroundColor = ConsoleColor.Red;
				Console.Write(" " + status);
				Console.CursorLeft = l;
				Console.CursorTop = t;
			}
			finally
			{
				Console.ForegroundColor = bkColor;		
			}
		}

		private static void ClearStatus(string status)
		{
			int l = Console.CursorLeft;
			int t = Console.CursorTop;

			if (string.IsNullOrEmpty(status))
				return;
			Console.Write(new string(' ', status.Length + 1));
			Console.CursorLeft = l;
			Console.CursorTop = t;
		}

		private static void DisplayCountDown(long ticks)
		{
			var lticks = Math.Abs(ticks);

			if (lticks >= TimeSpan.TicksPerSecond)
			{
				int l = Console.CursorLeft;
				int t = Console.CursorTop;

				var ts = TimeSpan.FromTicks(lticks);
				var totSecs = ts.Seconds;

				var bkColor = Console.ForegroundColor;
				try
				{
					for (int i = totSecs-1; i >= 0; --i)
					{
						ts -= TimeSpan.FromSeconds(1);
						Console.ForegroundColor = ConsoleColor.Red;
						Console.Write("Time: {0}", string.Format("{0:00}:{1:00}:{2:00}", ts.TotalHours, ts.Minutes, ts.Seconds));
						Console.CursorLeft = l;
						Console.CursorTop = t;
						Thread.Sleep(1000);
					}
				}
				finally
				{
					Console.ForegroundColor = bkColor;
					Console.Write(new string(' ', 14));
				}
			}

		}
	}
}