﻿using Gvr.StateMachine;
using Gvr.StateMachine.Extensions;
using StateMachineExamples.MultiStates;

namespace StateMachineExamples
{
	internal class MultiFsm : AutomatonBase<IMultiCmd>, IMultiCmd
	{
		public MultiFsm()
			: base(false)
		{
			// States definition
			var s0 = new S0(this, this);
			var s1 = new S1(this, this);
			var s2 = new S2(this, this);

			// Transitions
			this.addEdge(s0, S0.Signal1, s1);
			this.addEdge(s0, S0.Signal2, s2);
			this.addEdge(s1, S1.Signal1, s0);
			this.addEdge(s1, S1.Signal2, s0);
			this.addEdge(s2, S2.Signal1, s0);
			this.addEdge(s2, S2.Signal2, s0);

			SetInitialState(s0); // initial state
		}

		public void Do()
		{
			//var sl = new SmartLock();
			//sl.Lock(() => State.Do(), 500);

			//using(var sl = MonitorLock.CreateLock(SynchEvent))
			//{
			//    State.Do();
			//}

			Commons.LockWithTimeout(SynchEvent, 500, new Action(() => State.Do()));
		}
	}
}