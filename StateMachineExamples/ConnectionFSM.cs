﻿using System;
using System.Net.Sockets;
using System.Threading;
using Gvr.StateMachine;
using StateMachineExamples.ConnectionStates;

namespace StateMachineExamples
{
	/// <summary>
	/// Context is a class that encapsulates transition logic
	/// </summary>
	internal class ConnectionFsm : AutomatonBase<State>, IConnection
	{
		private DataModel _model;

		private ConnectionFsm()
			: this(false)
		{
		}

		private ConnectionFsm(bool ignoreInvalidTransitions)
			: base(ignoreInvalidTransitions)
		{
			_model = new DataModel();

			// initialize model
			_model.socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
		}

		#region Builder

		/// <summary>
		/// Create the state machine
		/// </summary>
		private void init()
		{
			// States definition
			State connectedState = new ConnectedState(this, this, _model);
			State disconnectedState = new DisconnectedState(this, this, _model);

			// Transitions
			this.addEdge(connectedState, ConnectedState.ERROR, disconnectedState);
			this.addEdge(connectedState, ConnectedState.DISCONNECT, disconnectedState);
			this.addEdge(disconnectedState, DisconnectedState.CONNECT, connectedState);
			this.addEdge(disconnectedState, DisconnectedState.ERROR, disconnectedState);

			SetInitialState(disconnectedState); // initial state
		}

		public static ConnectionFsm createAutomaton()
		{
			var fsm = new ConnectionFsm();
			fsm.init();
			return fsm;
		}

		#endregion

		#region State Machine specific mathods

		public bool isConnected
		{
			get { return State is ConnectedState; }
		}

		public void waitConnected()
		{
			lock (this)
			{
				// calls connect and blocks
				connect();
				System.Threading.Monitor.Wait(this);
			}
		}

		#endregion

		#region AutomantonBase overrides

		protected override void onEvent(Event ev)
		{
			Console.WriteLine("event arrived: {0}", ev);
		}

		protected override void onStateChanged(State oldState, State newState)
		{
			Console.WriteLine("state changed from {0} to {1}", oldState, newState);

			if (newState is ConnectedState)
			{
				lock (this)
				{
					// unblock waitConnected
					System.Threading.Monitor.PulseAll(this); // signal ConnectionState reached
				}
			}
			else // not connected
			{
				// execute a new connection retry in a new thread so the current thread (FSM transition) can completes
				ThreadPool.QueueUserWorkItem((o) => connect());
			}
		}

		protected override void onDispose(bool disposing)
		{
			if (disposing)
			{
				if (_model.socket != null && _model.socket.Connected)
				{
					_model.socket.Shutdown(SocketShutdown.Both);
					_model.socket.Close();
				}
			}
		}

		#endregion

		#region IConnection implementation

		public void connect()
		{
			lock (SynchEvent)
			{
				if (Disposed) return;

				Console.WriteLine("method invoked {0}", System.Reflection.MethodBase.GetCurrentMethod().Name);
				(State as IConnection).connect();
			}
		}

		public void disconnect()
		{
			lock (SynchEvent)
			{
				if (Disposed) return;

				Console.WriteLine("method invoked {0}", System.Reflection.MethodBase.GetCurrentMethod().Name);
				(State as IConnection).disconnect();
			}
		}

		public int receive()
		{
			lock (SynchEvent)
			{
				if (Disposed) return -1;

				Console.WriteLine("method invoked {0}", System.Reflection.MethodBase.GetCurrentMethod().Name);
				return (State as IConnection).receive();
			}
		}

		public void send(int value)
		{
			lock (SynchEvent)
			{
				if (Disposed) return;

				Console.WriteLine("method invoked {0}", System.Reflection.MethodBase.GetCurrentMethod().Name);
				(State as IConnection).send(value);
			}
		}

		#endregion
	}
}