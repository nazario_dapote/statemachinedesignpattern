﻿#define CSHARP2

#if CSHARP2

/// <summary>
/// Correctly add Action and Func<> delegate types to C#2.0
/// http://stackoverflow.com/questions/1701759/how-to-correctly-add-action-and-func-delegate-types-to-c2-0
/// </summary>
public delegate void Action();

public delegate void Action<T>(T t);

public delegate void Action<T, U>(T t, U u);

public delegate void Action<T, U, V>(T t, U u, V v);

public delegate TResult Func<TResult>();

public delegate TResult Func<T, TResult>(T t);

public delegate TResult Func<T, U, TResult>(T t, U u);

public delegate TResult Func<T, U, V, TResult>(T t, U u, V v);

#endif