﻿using System;
using System.IO;
using System.Security;

namespace Nada.Command
{
	/// <summary>
	/// Extension methods for the ConsoleColor enum.
	/// http://blog.lukesw.net/2009/08/consolecolor-extension-methods.html
	/// 
	/// Usage:
	/// using (ConsoleColor.Red.AsForeground())
	/// {
	///		Console.Error.WriteLine("Error… the application will now generate a lovely BSOD.");
	/// }
	/// </summary>
	public static class ConsoleColorExtensions
	{
		/// <summary>Provides an easy way to display messages in specified color.</summary>
		/// <param name="foregroundColor">The color in which the messages will be displayed.</param>
		/// <returns>An object which restores previous console foreground color on disposal.</returns>
		/// <example><code>
		/// using (ConsoleColor.Red.AsForeground())
		///     Console.WriteLine("This should be red.");
		///
		/// using (ConsoleColor.Cyan.AsForeground())
		/// using (ConsoleColor.Magenta.AsBackground())
		///     Console.WriteLine("This should be cyan on magenta background.");
		///
		/// Console.WriteLine("This should be displayed with default colors.");
		/// </code></example>
		public static IDisposable AsForeground(this ConsoleColor foregroundColor)
		{
			if (!Enum.IsDefined(typeof(ConsoleColor), foregroundColor))
				throw new ArgumentOutOfRangeException("foregroundColor");
			return new ConsoleColorizer(foregroundColor, true);
		}

		/// <summary>Provides an easy way to display messages on specified color.</summary>
		/// <param name="backgroundColor">The color on which the messages will be displayed.</param>
		/// <returns>An object which restores previous console background color on disposal.</returns>
		/// <example>See <see cref="M:ConsoleColorExtensions.AsForeground" /> for example.</example>
		public static IDisposable AsBackground(this ConsoleColor backgroundColor)
		{
			if (!Enum.IsDefined(typeof(ConsoleColor), backgroundColor))
				throw new ArgumentOutOfRangeException("backgroundColor");
			return new ConsoleColorizer(backgroundColor, false);
		}

		private sealed class ConsoleColorizer : IDisposable
		{
			public ConsoleColorizer(ConsoleColor cc, bool fore)
			{
				_fore = fore;
				try
				{
					_previousColor = _fore ? Console.ForegroundColor : Console.BackgroundColor;
					if (_fore) Console.ForegroundColor = cc;
					else Console.BackgroundColor = cc;
				}
				catch (Exception e)
				{
					if (!(e is ArgumentException || e is SecurityException || e is IOException)) throw;
				}
			}

			private bool _fore;
			private ConsoleColor? _previousColor;

			public void Dispose()
			{
				if (_previousColor.HasValue)
				{
					try
					{
						if (_fore) Console.ForegroundColor = _previousColor.Value;
						else Console.BackgroundColor = _previousColor.Value;
					}
					catch (Exception e)
					{
						if (!(e is ArgumentException || e is SecurityException || e is IOException)) throw;
					}
				}
				GC.SuppressFinalize(this);
			}
		}
	}
}
