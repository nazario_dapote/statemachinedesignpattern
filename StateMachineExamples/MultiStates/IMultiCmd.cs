﻿namespace StateMachineExamples.MultiStates
{
	internal interface IMultiCmd
	{
		void Do();
	}
}