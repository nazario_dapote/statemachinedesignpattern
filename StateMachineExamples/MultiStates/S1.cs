﻿using System;
using Gvr.StateMachine;
using Nada.Command;

namespace StateMachineExamples.MultiStates
{
	internal class S1 : StateBase<IMultiCmd>, IMultiCmd, IState
	{
		public static readonly Event Signal1 = new Event(MultiEvent.Signal1.ToString());
		public static readonly Event Signal2 = new Event(MultiEvent.Signal2.ToString());

		public S1(IMultiCmd fsm, IEventSink eventSink)
			: base("S1", fsm, eventSink)
		{
		}

		public void Do()
		{
			using (ConsoleColor.Yellow.AsForeground())
				Console.WriteLine("S1 do");
		}

		public void onEntry()
		{
			using (ConsoleColor.Green.AsForeground())
				Console.WriteLine("S1 entry");
		}

		public void onExit()
		{
			using (ConsoleColor.Green.AsForeground())
				Console.WriteLine("S1 exit");
		}
	}
}