﻿using System;
using Gvr.StateMachine;
using Nada.Command;

namespace StateMachineExamples.MultiStates
{
	internal class S0 : StateBase<IMultiCmd>, IMultiCmd, IState
	{
		public static readonly Event Signal1 = new Event(MultiEvent.Signal1.ToString());
		public static readonly Event Signal2 = new Event(MultiEvent.Signal2.ToString());

		public S0(IMultiCmd fsm, IEventSink eventSink)
			: base("S0", fsm, eventSink)
		{
		}

		public void Do()
		{
			using (ConsoleColor.Yellow.AsForeground())
				Console.WriteLine("S0 do");
		}

		public void onEntry()
		{
			Console.WriteLine("S0 entry");
		}

		public void onExit()
		{
			Console.WriteLine("S0 exit");
		}
	}
}