﻿using System;
using Gvr.StateMachine;
using Nada.Command;

namespace StateMachineExamples.MultiStates
{
	internal class S2 : StateBase<IMultiCmd>, IMultiCmd, IState
	{
		public static readonly Event Signal1 = new Event(MultiEvent.Signal1.ToString());
		public static readonly Event Signal2 = new Event(MultiEvent.Signal2.ToString());

		public S2(IMultiCmd fsm, IEventSink eventSink)
			: base("S2", fsm, eventSink)
		{
		}

		public void Do()
		{
			using (ConsoleColor.Yellow.AsForeground())
				Console.WriteLine("S2 do");
		}

		public void onEntry()
		{
			using (ConsoleColor.Red.AsForeground())
				Console.WriteLine("S2 entry");
		}

		public void onExit()
		{
			using (ConsoleColor.Red.AsForeground())
				Console.WriteLine("S2 exit");
		}
	}
}