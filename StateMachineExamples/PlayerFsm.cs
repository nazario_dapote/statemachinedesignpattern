﻿using System;
using Gvr.StateMachine;
using Gvr.StateMachine.Extensions;
using StateMachineExamples.PlayerStates;

namespace StateMachineExamples
{
	internal class PlayerFsm : AutomatonBase<IPlayerCommands>, IPlayerCommands
	{
		/// <summary>
		/// Event to indicate Description
		/// </summary>
		public event EventHandler Terminated;

		/// <summary>
		/// Called to signal to subscribers that Description
		/// </summary>
		/// <param name="e"></param>
		protected virtual void onTerminated(EventArgs e)
		{
			EventHandler eh = Terminated;
			if (eh != null)
				eh(this, e);
		}

		public PlayerFsm()
			: base(false)
		{
			// States definition
			var activeState = new ActiveState(this, this);
			var inactive = new InactiveState(this, this);
			var pausedState = new PausedState(this, this);
			var terminatedState = new TerminatedState(this, this);

			// Transitions
			this.addEdge(activeState, ActiveState.PauseCmd, pausedState);
			this.addEdge(activeState, ActiveState.End, inactive);
			this.addEdge(inactive, InactiveState.Exit, terminatedState);
			this.addEdge(inactive, InactiveState.Begin, activeState);
			this.addEdge(pausedState, PausedState.End, inactive);
			this.addEdge(pausedState, PausedState.Resume, activeState);

			SetInitialState(inactive); // initial state

			Console.WriteLine("Current state: {0}", State);
		}

		public void Play()
		{
			Commons.LockWithTimeout(SynchEvent, 500, () => State.Play(), (e) => Console.Error.WriteLine(e.Message));
		}

		public void Stop()
		{
			Commons.LockWithTimeout(SynchEvent, 500, () => State.Stop(), (e) => Console.Error.WriteLine(e.Message));
		}

		public void Pause()
		{
			Commons.LockWithTimeout(SynchEvent, 500, () => State.Pause(), (e) => Console.Error.WriteLine(e.Message));
		}

		protected override void onStateChanged(IPlayerCommands oldState, IPlayerCommands newState)
		{
			Console.WriteLine("state changed from {0} to {1}", oldState, newState);

			if (newState is TerminatedState)
				onTerminated(EventArgs.Empty);
		}
	}
}