﻿using System.Net.Sockets;
using Gvr.StateMachine;
using StateMachineExamples.ConnectionStates;

namespace StateMachineExamples
{
	public class PushBackConnection : AutomatonBase<IPushBackConnection>, IPushBackConnection
	{
		private DataModel _model;

		private PushBackConnection()
		{
			_model = new DataModel();

			// initialize model
			_model.socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
		}

		/// <summary>
		/// Create the state machine
		/// </summary>
		private void init()
		{
			// States definition
			IPushBackConnection connected = new PushBackConnectedState(this, this, _model);
			IPushBackConnection disconnected = new PushBackDisconnectedState(this, this, _model);

			// Transitions
			addEdge(connected, PushBackConnectedState.DISCONNECT, disconnected);
			addEdge(connected, PushBackConnectedState.ERROR, disconnected);
			addEdge(disconnected, PushBackDisconnectedState.CONNECT, connected);
			SetInitialState(disconnected); // initial state
		}

		public static IPushBackConnection createAutomaton()
		{
			var fsm = new PushBackConnection();
			fsm.init();
			return fsm;
		}

		public void connect()
		{
			State.connect();
		}

		public void disconnect()
		{
			State.disconnect();
		}

		public int receive()
		{
			return State.receive();
		}

		public void send(int value)
		{
			State.send(value);
		}

		public void pushBack(int value)
		{
			State.pushBack(value);
		}
	}
}