﻿using Gvr.StateMachine;
using StateMachineExamples.LightStates;

namespace StateMachineExamples
{
	internal class LightsFsm : AutomatonBase<ILights>, ILights
	{
		public LightsFsm()
			: base(false)
		{
			// create FSM
			var onState = new LightOnState(this, this);
			var offState = new LightOffState(this, this);

			this.addEdge(onState, LightOnState.SWITCH, offState);
			this.addEdge(offState, LightOffState.SWITCH, onState);

			this.SetInitialState(offState);
		}

		public void toggle()
		{
			State.toggle();
		}
	}
}