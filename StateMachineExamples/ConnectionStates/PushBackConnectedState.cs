﻿using System.Collections.Generic;
using Gvr.StateMachine;

namespace StateMachineExamples.ConnectionStates
{
	internal class PushBackConnectedState : ConnectedState, IPushBackConnection
	{
		private Stack<int> stack = new Stack<int>();

		public PushBackConnectedState(IConnection fsm, IEventSink eventSink, DataModel model)
			: base("PushBackConnectedState", fsm, eventSink, model)
		{
		}

		public override int receive()
		{
			if (stack.Count == 0)
			{
				return base.receive();
			}
			return stack.Pop();
		}

		public void pushBack(int value)
		{
			stack.Push(value);
		}
	}
}