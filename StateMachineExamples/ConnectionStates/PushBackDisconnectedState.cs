﻿using System;
using Gvr.StateMachine;

namespace StateMachineExamples.ConnectionStates
{
	internal class PushBackDisconnectedState : DisconnectedState, IPushBackConnection
	{
		public PushBackDisconnectedState(IConnection fsm, IEventSink eventSink, DataModel model)
			: base("PushBackDisconnectedState", fsm, eventSink, model)
		{
		}

		public void pushBack(int value)
		{
			throw new NotImplementedException();
		}
	}
}