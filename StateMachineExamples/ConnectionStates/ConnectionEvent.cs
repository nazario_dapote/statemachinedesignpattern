﻿namespace StateMachineExamples.ConnectionStates
{
	internal enum ConnectionEvent
	{
		Connected,
		Disconnected,
		Error,
	}
}