﻿using System;
using Gvr.StateMachine;

namespace StateMachineExamples.ConnectionStates
{
	/// <summary>
	/// Implements default behaviors for state
	/// </summary>
	internal abstract class State : StateBase<IConnection>, IConnection, IState
	{
		private DataModel _model;

		internal DataModel Model
		{
			get { return _model; }
		}

		public State(string name, IConnection fsm, IEventSink eventSink, DataModel model)
			: base(name, fsm, eventSink)
		{
			_model = model;
		}

		public virtual void connect()
		{
			throw new NotImplementedException();
		}

		public virtual void disconnect()
		{
			throw new NotImplementedException();
		}

		public virtual int receive()
		{
			throw new NotImplementedException();
		}

		public virtual void send(int value)
		{
			throw new NotImplementedException();
		}

		public virtual void onEntry()
		{
		}

		public virtual void onExit()
		{
		}
	}
}