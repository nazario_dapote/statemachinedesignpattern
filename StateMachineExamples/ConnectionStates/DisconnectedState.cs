﻿using System.IO;
using Gvr.StateMachine;

namespace StateMachineExamples.ConnectionStates
{
	internal class DisconnectedState : State
	{
		public static readonly Event CONNECT = new Event(ConnectionEvent.Connected.ToString());

		public static readonly Event ERROR = new Event(ConnectionEvent.Error.ToString());

		public DisconnectedState(IConnection fsm, IEventSink eventSink, DataModel model)
			: this("DisconnectedState", fsm, eventSink, model)
		{
		}

		protected DisconnectedState(string stateName, IConnection fsm, IEventSink eventSink, DataModel model)
			: base(stateName, fsm, eventSink, model)
		{
		}

		public override void connect()
		{
			try
			{
				Model.socket.Connect("localhost", 65200);
				castEvent(CONNECT);
			}
			catch
			{
				castEvent(ERROR);
			}
		}

		public override void disconnect()
		{
			// same state
		}

		public override int receive()
		{
			throw new IOException("Connection is closed (receive)");
		}

		public override void send(int value)
		{
			throw new IOException("Connection is closed (send)");
		}
	}
}