﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Threading;
using Gvr.StateMachine;
using Gvr.StateMachine.Extensions;

namespace StateMachineExamples.ConnectionStates
{
	internal class ConnectedState : State, IDisposable
	{
		public static readonly Event DISCONNECT = new Event(ConnectionEvent.Disconnected.ToString());
		public static readonly Event ERROR = new Event(ConnectionEvent.Error.ToString());

		private Timer _statusThread;
		private volatile bool _doNotRescheduleTimer;

		public ConnectedState(IConnection fsm, IEventSink eventSink, DataModel model)
			: this("ConnectedState", fsm, eventSink, model)
		{
		}

		protected ConnectedState(string name, IConnection fsm, IEventSink eventSink, DataModel model)
			: base(name, fsm, eventSink, model)
		{
		}

		/// <summary>
		/// check if socket is still connected
		/// Socket.Connected cannot be used in this situation.
		/// You need to poll connection every time to see if connection is still active.
		/// </summary>
		/// <param name="s"></param>
		/// <returns></returns>
		private static bool socketConnected(Socket s)
		{
			return s != null && !(s.Poll(1000, SelectMode.SelectRead) && s.Available == 0);
		}

		public override void connect()
		{
		}

		public override void disconnect()
		{
			try
			{
				Model.socket.Disconnect(false);
			}
			finally
			{
				castEvent(DISCONNECT);
			}
		}

		public override int receive()
		{
			try
			{
				return Model.socket.Receive(new byte[1024]);
			}
			catch (IOException)
			{
				castEvent(ERROR);
				throw;
			}
		}

		public override void send(int value)
		{
			try
			{
				Model.socket.Send(new byte[1024]);
			}
			catch (IOException)
			{
				castEvent(ERROR);
				throw;
			}
		}

		public override void onEntry()
		{
			Console.WriteLine("ConnectedState entry");

			_doNotRescheduleTimer = false;

			// Check connection active every 1 sec
			const int TIMER_PERIOD = 1000;

			if (_statusThread == null) // do not recreate timer if exists
			{
				_statusThread = Commons.TimerCallback(
					// action
					() => checkConnection(), 
					// period
					TIMER_PERIOD, 
					// onChange
					() => (_doNotRescheduleTimer) ? Timeout.Infinite : TIMER_PERIOD);
			}
		}

		private void checkConnection()
		{
			if (!socketConnected(Model.socket))
			{
				Model.socket.Shutdown(SocketShutdown.Both);
				Model.socket.Close();

				// socket is not reusable so create a new one
				Model.socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

				// signal connection lost
				castEvent(ERROR);
			}
		}

		public override void onExit()
		{
			Console.WriteLine("ConnectedState exit");

			// disable connection check
			_doNotRescheduleTimer = true;
		}

		#region IDisposable implementation

		private bool _disposed = false;

		//Implement IDisposable.
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (!_disposed)
			{
				if (disposing)
				{
					// Free other state (managed objects).
					if (_statusThread != null)
					{
						_statusThread.Dispose(); // do not reschedule timer
						_statusThread = null;
					}
				}
				// Free your own state (unmanaged objects).
				// Set large fields to null.
				_disposed = true;
			}
		}

		// Use C# destructor syntax for finalization code.
		~ConnectedState()
		{
			// Simply call Dispose(false).
			Dispose(false);
		}

		#endregion
	}
}