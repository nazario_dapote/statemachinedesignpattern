﻿namespace StateMachineExamples
{
	/// <summary>
	/// Demonstrate how extend the AI
	/// </summary>
	public interface IPushBackConnection : IConnection
	{
		void pushBack(int value);
	}
}