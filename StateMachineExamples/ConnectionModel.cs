﻿using System.Net.Sockets;

namespace StateMachineExamples
{
	/// <summary>
	/// is a class to provide a shared storage between the state classes.
	/// </summary>
	public class DataModel
	{
		public Socket socket;
	}
}