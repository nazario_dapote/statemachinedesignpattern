﻿/********************************************************************************
Author		: Nazario D'Apote
E-Mail		: cons_nazario.dapote@gilbarco.com
License		: Copyright © Gilbarco S.r.l. 2016
*********************************************************************************/

using System;
using System.Runtime.Serialization;
using System.Security.Permissions;
using Gvr.StateMachine.Extensions;

namespace Gvr.StateMachine
{
	[Serializable]
	public class AutomatonException : Exception
	{
		public AutomatonException()
		{
		}

		public AutomatonException(string message)
			: base(message)
		{
		}

		public AutomatonException(string message, params object[] args)
			: base(string.Format(message, args))
		{
		}

		public AutomatonException(string message, Exception inner)
			: base(message, inner)
		{
		}

		[SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
		protected AutomatonException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
			: base(info, context)
		{
		}

		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
				throw new ArgumentNullException(Commons.GetArgName(new { info }));

			// your code here

			// MUST call through to the base class to let it save its own state
			base.GetObjectData(info, context);
		}
	}
}