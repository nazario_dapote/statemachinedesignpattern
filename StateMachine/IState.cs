﻿/********************************************************************************
Author		: Nazario D'Apote
E-Mail		: cons_nazario.dapote@gilbarco.com
License		: Copyright © Gilbarco S.r.l. 2016
*********************************************************************************/

namespace Gvr.StateMachine
{
	public interface IState
	{
		void onEntry();

		void onExit();
	}
}