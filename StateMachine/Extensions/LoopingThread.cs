﻿/********************************************************************************
Author		: Nazario D'Apote
E-Mail		: cons_nazario.dapote@gilbarco.com
License		: Copyright © Gilbarco S.r.l. 2016
*********************************************************************************/

using System;
using System.Threading;

namespace Gvr.StateMachine.Extensions
{         
	/// <summary>
	/// Loops an action passed to the constructor.
	/// https://stackoverflow.com/questions/142826/is-there-a-way-to-indefinitely-pause-a-thread
	/// </summary>
	public class LoopingThread : IDisposable
	{
		private readonly Action _loopedAction;
		private readonly AutoResetEvent _pauseEvent;
		private readonly AutoResetEvent _resumeEvent;
		private readonly AutoResetEvent _stopEvent;
		private readonly AutoResetEvent _waitEvent;
		private readonly Thread _thread;
		private volatile bool _paused;

		public LoopingThread(Action loopedAction)
			: this(ThreadPriority.Normal, loopedAction)
		{
		}

		public LoopingThread(ThreadPriority priority, Action loopedAction)
		{
			_loopedAction = loopedAction;
			_thread = new Thread(Loop);
			_thread.Priority = priority;
			_pauseEvent = new AutoResetEvent(false);
			_resumeEvent = new AutoResetEvent(false);
			_stopEvent = new AutoResetEvent(false);
			_waitEvent = new AutoResetEvent(false);

			DefaultTimeout = 0; // immediate
		}

		public LoopingThread(Delegate loopedAction, params object[] args)
			: this(ThreadPriority.Normal, loopedAction, args)
		{
		}

		public LoopingThread(ThreadPriority priority, Delegate loopedAction, params object[] args)
			:this( new Action( () => { if(Commons.ValidateDelegate(loopedAction, args)) loopedAction.DynamicInvoke(args); } ))
		{
		}

		public int DefaultTimeout {	get; set; }

		public bool Start()
		{
			if (_thread.ThreadState == ThreadState.Unstarted)
				_thread.Start();

			return _thread.ThreadState == ThreadState.Running || _thread.ThreadState == ThreadState.Suspended;
		}

		public void Pause()
		{
			Pause(DefaultTimeout);
		}

		public void Pause(int timeout)
		{
			if (!_paused) // this will prevent double Pause call
			{
				_paused = true;
				_pauseEvent.Set();
				_waitEvent.WaitOne(timeout);
			}
		}

		public void Resume()
		{
			_resumeEvent.Set();
		}

		public bool Stop()
		{
			return Stop(DefaultTimeout);
		}

		public bool Stop(int timeout)
		{
			if (_thread.IsAlive)
			{
				_stopEvent.Set();
				_resumeEvent.Set();

				if (!_thread.Join(timeout))
				{
					_thread.Abort();
					_thread.Join();
				}
			}

			return _thread.ThreadState == ThreadState.Aborted || _thread.ThreadState == ThreadState.Stopped;
		}

		public void WaitForPause()
		{
			Pause(Timeout.Infinite);
		}

		public void WaitForStop()
		{
			Stop(Timeout.Infinite);
		}

		public int PauseBetween { get; set; }

		private void Loop()
		{
			do
			{
				try
				{
					_loopedAction();
				}
				catch (ThreadAbortException)
				{
					Thread.ResetAbort();
				}

				if (_pauseEvent.WaitOne(PauseBetween))
				{
					_waitEvent.Set(); // notify that pause was reached
					_resumeEvent.WaitOne(Timeout.Infinite);
					_paused = false;
				}
			} while (!_stopEvent.WaitOne(0));
		}

		#region IDisposable implementation

		private bool _disposed = false;

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (!_disposed)
			{
				if (disposing)
				{
					// Free other state (managed objects).
					Stop();
					this._pauseEvent.Close();
					this._resumeEvent.Close();
					this._stopEvent.Close();
					this._waitEvent.Close();
				}
				// Free your own state (unmanaged objects).
				// Set large fields to null.
				_disposed = true;
			}
		}

		// Use C# destructor syntax for finalization code.
		~LoopingThread()
		{
			// Simply call Dispose(false).
			Dispose(false);
		} 

		#endregion
	}
}
