﻿/********************************************************************************
Author		: Nazario D'Apote
E-Mail		: cons_nazario.dapote@gilbarco.com
License		: Copyright © Gilbarco S.r.l. 2016
*********************************************************************************/

using System;
using System.Runtime.CompilerServices;
using System.Threading;

namespace Gvr.StateMachine.Extensions
{
	public static class Commons
	{
		/// <summary>
		/// Get the name of method currently invoking this method
		/// </summary>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.NoInlining)]
		public static string GetCurrentMethod()
		{
			var st = new System.Diagnostics.StackTrace();
			var sf = st.GetFrame(1);

			return sf.GetMethod().Name;
		}

		[MethodImpl(MethodImplOptions.NoInlining)]
		public static string GetCallerMethod()
		{
			var st = new System.Diagnostics.StackTrace();
			var sf = st.GetFrame(2);

			return sf.GetMethod().Name;
		}

		/// <summary>
		/// Return the name of the argument
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="item"></param>
		/// <returns></returns>
		public static string GetArgName<T>(T item)
			where T : class
		{
			var properties = typeof(T).GetProperties();
			if (properties.Length > 1)
				return properties[0].Name;
			else
				return string.Empty;
		}

		/// <summary>
		/// Reports and exception to a callback and doesn't re-throw Exception in action
		/// </summary>
		/// <typeparam name="T">Exception type</typeparam>
		/// <param name="action">action to invoke</param>
		/// <param name="exception">exception to report</param>
		public static void SafeCallbackError<T>(Action<T> action, T exception)
			where T : class
		{
			try
			{
				if (action == null)
					throw new ArgumentNullException(GetArgName(new { action }));

				if (exception == null)
					throw new ArgumentNullException(GetArgName(new { exception }));

				action(exception);
			}
			catch (Exception e)
			{
				System.Diagnostics.Trace.WriteLine(e.Message);
#if DEBUG
				System.Diagnostics.Debugger.Break();
#endif
			}
		}

		/// <summary>
		/// Execute a specified action until success or timeout
		/// </summary>
		/// <param name="action">Action to execute</param>
		/// <param name="timeout">action timeout</param>
		/// <param name="onError">Callback on error thrown (null, no error notify)</param>
		/// <returns></returns>
		public static bool TimeoutCallback(Action action, TimeSpan timeout, Action<Exception> onError = null)
		{
			return TimeoutCallback((Delegate)action, timeout.Milliseconds, onError);
		}

		/// <summary>
		/// Execute a specified action until success or timeout
		/// </summary>
		/// <param name="action">Action to execute</param>
		/// <param name="timeout">action timeout</param>
		/// <param name="onError">Callback on error thrown (null, no error notify)</param>
		/// <returns></returns>
		public static bool TimeoutCallback(Action action, int timeout, Action<Exception> onError = null)
		{
			return TimeoutCallback((Delegate)action, timeout, onError);
		}

		/// <summary>
		/// Execute a specified action until success or timeout
		/// </summary>
		/// <param name="action">Action to execute</param>
		/// <param name="timeout">action timeout</param>
		/// <param name="onError">Callback on error thrown (null, no error notify)</param>
		/// <param name="args">arguments to the action</param>
		/// <returns></returns>
		public static bool TimeoutCallback(Delegate action, TimeSpan timeout, Action<Exception> onError = null, params object[] args)
		{
			return TimeoutCallback(action, timeout.Milliseconds, onError, args);
		}

		/// <summary>
		/// Execute a specified action until success or timeout
		/// </summary>
		/// <param name="action">Action to execute</param>
		/// <param name="timeout">action timeout</param>
		/// <param name="onError">Callback on error thrown (null, no error notify)</param>
		/// <param name="args">arguments to the action</param>
		/// <returns></returns>
		public static bool TimeoutCallback(Delegate action, int timeout, Action<Exception> onError = null, params object[] args)
		{
			bool result = false;

			var th = new Thread(() =>
			{
				try
				{
					SafeCallback(action, onError, args);
					result = true;
				}
				catch (ThreadAbortException)
				{
					Thread.ResetAbort();
				}
			});

			th.Start();

			// wait completed
			if (!th.Join(timeout))
			{
				th.Abort();
				th.Join();
			}

			return result;
		}

		/// <summary>
		/// Create a thread from pool and execute action
		/// </summary>
		/// <param name="action">action to execute</param>
		/// <param name="onError">delegate to call on action exception</param>
		/// <returns>an handle to the semaphore handle</returns>
		public static WaitHandle ThreadPoolCallback(Action action, Action<Exception> onError = null)
		{
			return ThreadPoolCallback((Delegate)action, onError);
		}

		/// <summary>
		/// Create a thread from pool and execute action
		/// </summary>
		/// <param name="action">action to execute</param>
		/// <param name="onError">delegate to call on action exception</param>
		/// <param name="args">arguments for action delegate</param>
		/// <returns>an handle to the semaphore handle</returns>
		public static WaitHandle ThreadPoolCallback(Delegate action, Action<Exception> onError = null, params object[] args)
		{
			var waitingHandle = new ManualResetEvent(false);

			ThreadPool.QueueUserWorkItem((o) =>
			{
				SafeCallback(action, onError, args);
				((ManualResetEvent)o).Set();
			}, waitingHandle);

			return waitingHandle;
		}

		/// <summary>
		/// Create a thread from pool and execute action
		/// </summary>
		/// <param name="action">action to execute</param>
		/// <param name="onError">delegate to call on action exception</param>
		/// <returns>an handle to the IAsyncResult handle</returns>
		public static IAsyncResult AsyncCallback(Action action, Action<Exception> onError = null)
		{
			return AsyncCallback((Delegate)action, onError);
		}

		/// <summary>
		/// Create a thread from pool and execute action
		/// </summary>
		/// <param name="action">action to execute</param>
		/// <param name="onError">delegate to call on action exception</param>
		/// <param name="args">arguments for action delegate</param>
		/// <returns>an handle to the IAsyncResult handle</returns>
		public static IAsyncResult AsyncCallback(Delegate action, Action<Exception> onError = null, params object[] args)
		{
			var asyncDoneTime = DateTime.MinValue;
			Action asyncAction = () => { SafeCallback(action, onError, args); };
			return asyncAction.BeginInvoke(ar =>
				{
					asyncAction.EndInvoke(ar);
					asyncDoneTime = DateTime.Now;
				}, null);
		}

		/// <summary>
		/// Create a timer and execute and action every period
		/// </summary>
		/// <param name="action">action to execute on timer triggered</param>
		/// <param name="period">period of the timer</param>
		/// <param name="onChange">delegate to execute after action to change timer period</param>
		/// <param name="onError">delegate to execute on action error (can be null)</param>
		public static Timer TimerCallback(Action action, int period, Func<int> onChange = null, Action<Exception> onError = null)
		{
			return TimerCallback((Delegate)action, period, onChange, onError);
		}

		/// <summary>
		/// Create a timer and execute and action every period
		/// </summary>
		/// <param name="action">action to execute on timer triggered</param>
		/// <param name="period">period of the timer</param>
		/// <param name="onChange">delegate to execute after action to change timer period</param>
		/// <param name="onError">delegate to execute on action error (can be null)</param>
		/// <param name="args">arguments for action delegate</param>
		/// <returns></returns>
		public static Timer TimerCallback(Delegate action, int period, Func<int> onChange = null, Action<Exception> onError = null, params object[] args)
		{
			if (action == null)
				throw new ArgumentNullException(GetArgName(new { action }));

			Timer th = null;

			th = new Timer(new TimerCallback((o) =>	{

				SafeCallback(action, onError, args);
				try
				{
					if (th != null)
					{
						if (onChange != null) th.Change(onChange(), 0);
						else th.Change(period, 0);
					}					
				}
				catch (ObjectDisposedException)
				{
					System.Diagnostics.Trace.WriteLine("[{0}] Timer disposed!", GetCurrentMethod());
				}
			}), null, period, 0);

			return th;
		}

		/// <summary>
		/// Invoke a callback e catch exception if thrown to a callback onError
		/// </summary>
		/// <param name="action">Action to invoke and trap</param>
		/// <param name="onError">Callback on exception thrown by action</param>
		public static void SafeCallback(Action action, Action<Exception> onError = null)
		{
			SafeCallback((Delegate)action, onError);
		}

		/// <summary>
		/// Invoke a callback e catch exception if thrown to a callback onError
		/// </summary>
		/// <param name="action">Action to invoke and trap</param>
		/// <param name="onError">Callback on exception thrown by action</param>
		/// <param name="args">arguments to pass to action</param>
		public static void SafeCallback(Delegate action, Action<Exception> onError = null, params object[] args)
		{
#if DEBUG
			if (action == null)
				throw new ArgumentNullException(GetArgName(new { action }));

			if (!ValidateDelegate(action, args))
				throw new ArgumentOutOfRangeException();
#endif

			try
			{
				action.DynamicInvoke(args);
			}
			catch (Exception excpt)
			{
				if (onError != null)
					SafeCallbackError(onError, excpt);
			}
		}

		/// <summary>
		/// Check arguments valid to call delegate
		/// </summary>
		/// <param name="method"></param>
		/// <param name="args"></param>
		/// <returns></returns>
		public static bool ValidateDelegate(Delegate method, params object[] args)
		{
			var parameters = method.Method.GetParameters();
			if (parameters.Length != args.Length) 
				return false;

			for (int i = 0; i < parameters.Length; ++i)
			{
				if (parameters[i].ParameterType.IsValueType && args[i] == null || !parameters[i].ParameterType.IsAssignableFrom(args[i].GetType()))
				{
					return false;
				}
			}

			return true;
		}

		/// <summary>
		/// Simple lock acquirer with timeout
		/// </summary>
		/// <param name="oLock">lock object</param>
		/// <param name="timeout">timeout</param>
		/// <param name="action">action to execute on lock acquired</param>
		/// <param name="onError">delegate to execute on action error</param>
		public static void LockWithTimeout(object oLock, int timeout, Action action, Action<Exception> onError = null)
		{
			LockWithTimeout(oLock, timeout, (Delegate) action, onError);
		}

		/// <summary>
		/// Simple lock acquirer with timeout
		/// </summary>
		/// <param name="oLock">lock object</param>
		/// <param name="timeout">timeout</param>
		/// <param name="action">action to execute on lock acquired</param>
		/// <param name="onError">delegate to execute on action error (can be null)</param>
		/// <param name="args">arguments for action delegate</param>
		/// <exception cref="TimeoutException">on timeout exceeded</exception>
		public static void LockWithTimeout(object oLock, int timeout, Delegate action, Action<Exception> onError = null, params object[] args)
		{
			if (!TryLockWithTimeout(oLock, timeout, action, onError, args))
				throw new TimeoutException("Timeout exceeded, unable to lock.");
		}

		/// <summary>
		/// Try to get a lock to an object with timeout
		/// </summary>
		/// <param name="oLock">lock object</param>
		/// <param name="timeout">timeout</param>
		/// <param name="action">action to execute on lock acquired</param>
		/// <param name="onError">delegate to execute on action error  (can be null)</param>
		public static bool TryLockWithTimeout(object oLock, int timeout, Action action, Action<Exception> onError = null)
		{
			return TryLockWithTimeout(oLock, timeout, (Delegate)action, onError, null);
		}

		/// <summary>
		/// Try to get a lock to an object with timeout
		/// </summary>
		/// <param name="oLock">lock object</param>
		/// <param name="timeout">timeout</param>
		/// <param name="action">action to execute on lock acquired</param>
		/// <param name="onError">delegate to execute on action error  (can be null)</param>
		/// <param name="args">arguments for action delegate</param>
		public static bool TryLockWithTimeout(object oLock, int timeout, Delegate action, Action<Exception> onError = null, params object[] args)
		{
			if (System.Threading.Monitor.TryEnter(oLock, timeout))
			{
				try
				{
					SafeCallback(action, onError, args);
					return true;
				}
				finally
				{
					System.Threading.Monitor.Exit(oLock);
				}
			}
			return false;
		}
	}
}