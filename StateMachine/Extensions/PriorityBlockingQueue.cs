/********************************************************************************
Author		: Nazario D'Apote
E-Mail		: cons_nazario.dapote@gilbarco.com
License		: Copyright � Gilbarco S.r.l. 2016
*********************************************************************************/
using System;
using System.Collections;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Threading;

namespace Gvr.StateMachine.Extensions
{
	/// <summary>
	/// Priority blocking queue class.
	/// Original version from: http://www.boyet.com/Articles/WritingapriorityqueueinC.html
	/// </summary>
	[Serializable()]
	public class PriorityBlockingQueue<T> : ICollection, ISerializable
	{
		private int count;
		private int capacity;
		private int version;
		private HeapEntry<T>[] heap;

		private const string capacityName = "capacity";
		private const string countName = "count";
		private const string heapName = "heap";

		private readonly Semaphore _semaphore = new Semaphore(0, Int32.MaxValue);

		public PriorityBlockingQueue()
		{
			capacity = 15; // 15 is equal to 4 complete levels
			heap = new HeapEntry<T>[capacity];
		}

		protected PriorityBlockingQueue(SerializationInfo info, StreamingContext context)
		{
			lock (heap)
			{
				capacity = info.GetInt32(capacityName);
				count = info.GetInt32(countName);
				var heapCopy = (HeapEntry<T>[])info.GetValue(heapName, typeof(HeapEntry<T>[]));
				heap = new HeapEntry<T>[capacity];
				Array.Copy(heapCopy, 0, heap, 0, count);
				version = 0;
			}
		}

		public bool Dequeue(out T item)
		{
			if (!_semaphore.WaitOne(1000))
			{
				item = default(T);
				return false;
			}

			lock (heap)
			{
				if (heap.Length == 0)
				{
					item = default(T);
					return false;
				}

				T result = heap[0].Item;
				count--;
				trickleDown(0, heap[count]);
				heap[count].Clear();
				version++;
				item = result;
				return true;
			}
		}

		public void Enqueue(T item, IComparable priority)
		{
			if (priority == null)
				throw new ArgumentNullException(Commons.GetArgName( new { item }));

			lock (heap)
			{
				if (count == capacity)
					growHeap();
				count++;
				bubbleUp(count - 1, new HeapEntry<T>(item, priority));
				version++;

				_semaphore.Release();
			}
		}

		private void bubbleUp(int index, HeapEntry<T> he)
		{
			int parent = getParent(index);
			// note: (index > 0) means there is a parent
			while ((index > 0) &&
				  (heap[parent].Priority.CompareTo(he.Priority) < 0))
			{
				heap[index] = heap[parent];
				index = parent;
				parent = getParent(index);
			}
			heap[index] = he;
		}

		private int getLeftChild(int index)
		{
			return (index * 2) + 1;
		}

		private int getParent(int index)
		{
			return (index - 1) / 2;
		}

		private void growHeap()
		{
			capacity = (capacity * 2) + 1;
			var newHeap = new HeapEntry<T>[capacity];
			System.Array.Copy(heap, 0, newHeap, 0, count);
			heap = newHeap;
		}

		private void trickleDown(int index, HeapEntry<T> he)
		{
			int child = getLeftChild(index);
			while (child < count)
			{
				if (((child + 1) < count) &&
					(heap[child].Priority.CompareTo(heap[child + 1].Priority) < 0))
				{
					child++;
				}
				heap[index] = heap[child];
				index = child;
				child = getLeftChild(index);
			}
			bubbleUp(index, he);
		}

		#region IEnumerable implementation

		public IEnumerator GetEnumerator()
		{
			return new PriorityQueueEnumerator<T>(this);
		}

		#endregion

		#region ICollection implementation

		public int Count
		{
			get { return count; }
		}

		public void CopyTo(Array array, int index)
		{
			System.Array.Copy(heap, 0, array, index, count);
		}

		public object SyncRoot
		{
			get { return this; }
		}

		public bool IsSynchronized
		{
			get { return false; }
		}

		#endregion

		#region ISerializable implementation

		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			lock (heap)
			{
				info.AddValue(capacityName, capacity);
				info.AddValue(countName, count);
				var heapCopy = new HeapEntry<T>[count];
				Array.Copy(heap, 0, heapCopy, 0, count);
				info.AddValue(heapName, heapCopy, typeof(HeapEntry<T>[]));
			}
		}

		#endregion

		#region Priority Queue enumerator

		[Serializable()]
		private class PriorityQueueEnumerator<V> : IEnumerator
		{
			private int index;
			private PriorityBlockingQueue<V> pq;
			private int version;

			public PriorityQueueEnumerator(PriorityBlockingQueue<V> pq)
			{
				this.pq = pq;
				Reset();
			}

			private void checkVersion()
			{
				if (version != pq.version)
					throw new InvalidOperationException();
			}

			#region IEnumerator Members

			public void Reset()
			{
				index = -1;
				version = pq.version;
			}

			public object Current
			{
				get
				{
					checkVersion();
					return pq.heap[index].Item;
				}
			}

			public bool MoveNext()
			{
				checkVersion();
				if (index + 1 == pq.count)
					return false;
				index++;
				return true;
			}

			#endregion
		}

		#endregion
	}

	[Serializable()]
	public struct HeapEntry<T>
	{
		private T item;
		private IComparable priority;

		public HeapEntry(T item, IComparable priority)
		{
			this.item = item;
			this.priority = priority;
		}

		public T Item
		{
			get { return item; }
		}

		public IComparable Priority
		{
			get { return priority; }
		}

		public void Clear()
		{
			item = default(T);
			priority = null;
		}
	}

	[Serializable()]
	public class ReversePriority : IComparable
	{
		private int priority;

		public ReversePriority(int priority)
		{
			this.priority = priority;
		}

		public int CompareTo(object obj)
		{
			ReversePriority castObj = obj as ReversePriority;
			if (castObj == null)
				throw new InvalidCastException();
			if (priority < castObj.priority)
				return 1;
			if (priority == castObj.priority)
				return 0;
			return -1;
		}
	}
}