﻿/********************************************************************************
Author		: Nazario D'Apote
E-Mail		: cons_nazario.dapote@gilbarco.com
License		: Copyright © Gilbarco S.r.l. 2016
*********************************************************************************/

using System;
using System.Diagnostics;
using System.Threading;

namespace Gvr.StateMachine.Extensions
{         
	/// <summary>
	/// Identify the thread which holds the lock
	/// https://stackoverflow.com/questions/3523544/identify-the-thread-which-holds-the-lock
	/// </summary>
	/// <example>
	/// create an object to lock on
	/// private readonly object _requestLock = new LockObject("_requestLock");
	///
	/// using (MonitorLock.CreateLock(_requestLock))
	/// {
	/// 	//do some work
	/// }
	/// </example>
	public class MonitorLock : IDisposable
	{
		public static MonitorLock CreateLock(object value)
		{
			return new MonitorLock(value);
		}

		private readonly object _l;

		protected MonitorLock(object l)
		{
			_l = l;

			Trace.WriteLine(string.Format("Lock {0} attempt by {2} thread {1}", _l, Thread.CurrentThread.ManagedThreadId, getLockName()));

			Monitor.Enter(_l);

			Trace.WriteLine(string.Format("Lock {0} held by {2} thread {1}", _l, Thread.CurrentThread.ManagedThreadId, getLockName()));
		}

		private string getLockName()
		{
			var objName = string.Empty;
			var o = _l as LockObject;
			if (o != null)
				objName = o.Name;

			return objName;
		}

		#region IDisposable implementation
		private bool _disposed = false;

		//Implement IDisposable.
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (!_disposed)
			{
				if (disposing)
				{
					// Free other state (managed objects).
					Monitor.Exit(_l);

					Trace.WriteLine(string.Format("Lock {0} released by {2} thread {1}", _l, Thread.CurrentThread.ManagedThreadId, getLockName()));
				}
				// Free your own state (unmanaged objects).
				// Set large fields to null.
				_disposed = true;
			}
		}

		// Use C# destructor syntax for finalization code.
		~MonitorLock()
		{
			// Simply call Dispose(false).
			Dispose(false);
		}
		#endregion
	}
}