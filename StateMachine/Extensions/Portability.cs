﻿/********************************************************************************
Author		: Nazario D'Apote
E-Mail		: cons_nazario.dapote@gilbarco.com
License		: Copyright © Gilbarco S.r.l. 2016
*********************************************************************************/

namespace Gvr.StateMachine
{
#if LINQBRIDGE_LIB
#else

	public delegate void Action();

	public delegate void Action<T1, T2>(T1 arg1, T2 arg2);

	public delegate void Action<T1, T2, T3>(T1 arg1, T2 arg2, T3 arg3);

	public delegate void Action<T1, T2, T3, T4>(T1 arg1, T2 arg2, T3 arg3, T4 arg4);

	public delegate TResult Func<TResult>();

	public delegate TResult Func<T, TResult>(T t);

	public delegate TResult Func<T, U, TResult>(T t, U u);

	public delegate TResult Func<T, U, V, TResult>(T t, U u, V v);

#endif
}