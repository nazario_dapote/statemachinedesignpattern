﻿/********************************************************************************
Author		: Nazario D'Apote
E-Mail		: cons_nazario.dapote@gilbarco.com
License		: Copyright © Gilbarco S.r.l. 2016
*********************************************************************************/

using System;
using System.Diagnostics;
using System.Threading;

namespace Gvr.StateMachine.Extensions
{
	/// <summary>
	/// Allows to identify the thread which holds the lock
	/// https://stackoverflow.com/questions/3523544/identify-the-thread-which-holds-the-lock
	/// </summary>
	public class SmartLock
	{
		private object LockObject = new object();
		private string HoldingTrace = "";

		private const int WARN_TIMEOUT_MS = 5000; //! 5 seconds (default)
		private int _timeout;

		public void Lock(Action action, int timeoutWarning = WARN_TIMEOUT_MS)
		{
			_timeout = timeoutWarning;

			try
			{
				Enter();
				action.Invoke();
			}
			catch (Exception ex)
			{
				Trace.TraceError("SmartLock Lock action", ex);
			}
			finally
			{
				Exit();
			}
		}

		private void Enter()
		{
			try
			{
				bool locked = false;
				int timeoutMS = 0;
				while (!locked)
				{
					//keep trying to get the lock, and warn if not accessible after timeout
					locked = Monitor.TryEnter(LockObject, WARN_TIMEOUT_MS);
					if (!locked)
					{
						timeoutMS += _timeout;
						Trace.TraceWarning(string.Format("Lock held: {0} secs by {1} requested by {2}", (timeoutMS / 1000), HoldingTrace, GetStackTrace()));
					}
				}

				//save a stack trace for the code that is holding the lock
				HoldingTrace = GetStackTrace();
			}
			catch (Exception ex)
			{
				Trace.TraceError("SmartLock Enter", ex);
			}
		}

		private string GetStackTrace()
		{
			var trace = new StackTrace();
			string threadID = Thread.CurrentThread.Name ?? "";
			return string.Format("[{0}] {1}", threadID, trace.ToString().Replace('\n', '|').Replace("\r", ""));
		}

		private void Exit()
		{
			try
			{
				HoldingTrace = "";
				Monitor.Exit(LockObject);
			}
			catch (Exception ex)
			{
				Trace.TraceError("SmartLock Exit", ex);
			}
		}
	}
}