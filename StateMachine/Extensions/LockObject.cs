﻿/********************************************************************************
Author		: Nazario D'Apote
E-Mail		: cons_nazario.dapote@gilbarco.com
License		: Copyright © Gilbarco S.r.l. 2016
*********************************************************************************/

namespace Gvr.StateMachine.Extensions
{
	public class LockObject
	{
		public string Name { get; set; }

		public LockObject(string name)
		{
			Name = name;
		}

		public override string ToString()
		{
			return Name;
		}
	}
}
