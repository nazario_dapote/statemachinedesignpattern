﻿/********************************************************************************
Author		: Nazario D'Apote
E-Mail		: cons_nazario.dapote@gilbarco.com
License		: Copyright © Gilbarco S.r.l. 2016
*********************************************************************************/
using System;
using System.Threading;

namespace Gvr.StateMachine.Extensions
{
	/// <summary>
	/// A class to to implement a thread-safe producer-consumer scenarios
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public abstract class WorkQueue<T> : IDisposable
	{
		protected int QUEUE_THREAD_ABORT_TIMEOUT = 5000;
		private LoopingThread _processThread;
		private BlockingQueue<T> _workQueue;
		private volatile bool _paused;

		protected WorkQueue()
			: this(ThreadPriority.BelowNormal)
		{
		}

		protected WorkQueue(ThreadPriority priority)
		{
			this._workQueue = new BlockingQueue<T>();
			_paused = false;
			this._processThread = new LoopingThread(priority, () => processWorkQueue());
		}

		/// <summary>
		/// Start the queue processor
		/// </summary>
		public virtual bool Start()
		{
			return _processThread.Start();
		}

		/// <summary>
		/// Pause the queue processor
		/// </summary>
		public virtual void Pause()
		{
			_processThread.Pause();
			_paused = true;
		}

		/// <summary>
		/// Resume the queue processor
		/// </summary>
		public virtual void Resume()
		{
			_processThread.Resume();
			_paused = false;
		}

		/// <summary>
		/// Stop the queue processor
		/// </summary>
		/// <exception cref="">Exception("Can't stop the queue processor thread!")</exception>
		public virtual bool Stop()
		{
			return _processThread.Stop(QUEUE_THREAD_ABORT_TIMEOUT);
		}

		public virtual void AddWork(T work)
		{
			this._workQueue.Enqueue(work);
		}

		/// <summary>
		/// Process queued work.
		/// </summary>
		private void processWorkQueue()
		{
			// Wait for some work.
			T work;

			if (_paused)
				return; // for fast response

			// If we have work...
			if (this._workQueue.Dequeue(out work))
			{
				if (_paused)
					return; // for fast response

				doWork(work);
			}
		}

		protected abstract void doWork(T item);

		#region IDisposable implementation

		private bool _disposed = false;

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (!_disposed)
			{
				if (disposing)
				{
					// Free other state (managed objects).
					Stop();

					this._processThread.Dispose();
				}
				// Free your own state (unmanaged objects).
				// Set large fields to null.
				_disposed = true;
			}
		}

		// Use C# destructor syntax for finalization code.
		~WorkQueue()
		{
			// Simply call Dispose(false).
			Dispose(false);
		} 

		#endregion
	} 
}
