﻿/********************************************************************************
Author		: Nazario D'Apote
E-Mail		: cons_nazario.dapote@gilbarco.com
License		: Copyright © Gilbarco S.r.l. 2016
*********************************************************************************/

using System;

namespace Gvr.StateMachine
{
	/// <summary>
	/// base class for all state classes
	/// this allows to define a name for logging operations, allows to send event in the state, and has an handle the to container FSM (automaton)
	/// </summary>
	/// <typeparam name="AI"></typeparam>
	public abstract class StateBase<AI>
	{
		private string _name;

		private readonly AI _fsm;

		/// <summary>
		/// Return the State Machine that own the state
		/// </summary>
		public AI Owner
		{
			get { return _fsm; }
		}

		private readonly IEventSink _eventSink;

		protected StateBase(string name, AI fsm, IEventSink eventSink)
		{
			if (string.IsNullOrEmpty(name) || fsm == null || eventSink == null)
				throw new ArgumentNullException();

			this._name = name;
			this._fsm = fsm;
			this._eventSink = eventSink;
		}

		/// <summary>
		/// a lock must be acquired in every thread running in states
		/// </summary>
		public object SynchEvent
		{
			get { return _eventSink.SynchEvent; }
		}

		protected void castEvent(Event ev)
		{
			_eventSink.castEvent(ev);
		}

		public override string ToString()
		{
			return _name;
		}
	}
}