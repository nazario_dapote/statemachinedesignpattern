﻿/********************************************************************************
Author		: Nazario D'Apote
E-Mail		: cons_nazario.dapote@gilbarco.com
License		: Copyright © Gilbarco S.r.l. 2016
*********************************************************************************/

using System;

namespace Gvr.StateMachine
{
	/// <summary>
	/// Events (event1_1, event1_2, ...) — initiated by the state classes and passed to the context
	/// that does a transition depending on the event and the current state
	/// </summary>
	public sealed class Event
	{
		private readonly String _name;

		public Event(String name)
		{
			if (name == null)
				throw new NullReferenceException();
			_name = name;
		}

		public override string ToString()
		{
			return _name;
		}

		public override bool Equals(object obj)
		{
			if (object.ReferenceEquals(this, obj))
				return true;

			var other = obj as Event;
			if (other != null)
				return this._name.Equals(other._name);

			return base.Equals(obj);
		}

		public override int GetHashCode()
		{
			return _name.GetHashCode();
		}
	}
}