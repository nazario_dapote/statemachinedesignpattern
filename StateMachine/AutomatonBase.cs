﻿/********************************************************************************
Author		: Nazario D'Apote
E-Mail		: cons_nazario.dapote@gilbarco.com
License		: Copyright © Gilbarco S.r.l. 2016
*********************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using Gvr.StateMachine.Extensions;

namespace Gvr.StateMachine
{
	/// <summary>
	/// base class for all automata.
	/// AI (automata interface) is implemented by the context and is the only way of interaction between the automata and a client.
	/// This interface is also implemented by state classes.
	/// </summary>
	/// <typeparam name="AI"></typeparam>
	public abstract class AutomatonBase<AI> : IEventSink, IDisposable
		where AI : class
	{
		private bool _initStateAlreadySet;						// initial state should be set only one
		private AI _currentState;								// the current active state
		private IDictionary<AI, Dictionary<Event, AI>> _edges;	// map for transitions
		private bool _ignoreInvalidTransitions;					// invalid transitions throw exceptions?
		private readonly object _syncEvent = new object();		// object for thread synchronization

		protected AI State
		{
			get { lock (SynchEvent) { return _currentState; } }
		}

		protected void SetInitialState(AI state)
		{
			if (state == null)
				throw new ArgumentNullException(Commons.GetArgName(new { state }));

			if (!_initStateAlreadySet)
			{
				lock (_syncEvent)
				{
#if DEBUG
					System.Diagnostics.Trace.WriteLine(
						String.Format("Lock acquired by '{0}' on thread {1}", Commons.GetCallerMethod(), System.Threading.Thread.CurrentThread.ManagedThreadId));
#endif
					if (!_initStateAlreadySet) // double lock check
					{
						_initStateAlreadySet = true;

						if (_edges.ContainsKey(state))
						{
							performStateEntry(state);
							_currentState = state;

							// Notify blocked thread that was transition completed
							System.Threading.Monitor.PulseAll(_syncEvent);
						}
						else
							throw new AutomatonException("unknown state: {0}", state);
					}
				}
			}
			else
				throw new AutomatonException("initial state already set");
		}

		/// <summary>
		/// Used to synchronize threads with state transitions
		/// </summary>
		public object SynchEvent
		{
			get 
			{
#if DEBUG
				System.Diagnostics.Trace.WriteLine(
					String.Format("SynchEvent requested by '{0}' on thread {1}", Commons.GetCallerMethod(), System.Threading.Thread.CurrentThread.ManagedThreadId));
#endif
				return _syncEvent;
			}
		}

		public AutomatonBase()
			: this(false)
		{
		}

		public AutomatonBase(bool ignoreInvalidTransitions)
		{
			_edges = new Dictionary<AI, Dictionary<Event, AI>>();
			_ignoreInvalidTransitions = ignoreInvalidTransitions;
		}

		/// <summary>
		/// Set the state graph on transitions
		/// </summary>
		/// <param name="source"></param>
		/// <param name="ev"></param>
		/// <param name="target"></param>
		protected void addEdge(AI source, Event ev, AI target)
		{
			if (source == null || target == null || ev == null)
				throw new ArgumentNullException();

			throwIfDisposed();

			Dictionary<Event, AI> row = null;
			if (_edges.ContainsKey(source))
			{
				row = _edges[source];
			}
			else
			{
				row = new Dictionary<Event, AI>();
				_edges.Add(source, row);
			}
			row.Add(ev, target);
		}

		/// <summary>
		/// Called when a new event arrives
		/// this method must not block on SynchEvent
		/// </summary>
		/// <param name="ev"></param>
		protected virtual void onEvent(Event ev)
		{
			// implemented in you FSM
		}

		/// <summary>
		/// Called before a state change occurs
		/// this method must not block on SynchEvent
		/// </summary>
		/// <param name="currentState"></param>
		/// <param name="nextState"></param>
		/// <param name="param name="e">allows to prevent state transition</param>
		protected virtual void onStateChanging(AI currentState, AI nextState, CancelEventArgs e)
		{
			// implemented in you FSM
		}

		/// <summary>
		/// Called after a state change has occurred
		/// this method must not block on SynchEvent
		/// </summary>
		/// <param name="oldState"></param>
		/// <param name="newState"></param>
		protected virtual void onStateChanged(AI oldState, AI newState)
		{
			// implemented in you FSM
		}

		/// <summary>
		/// Called on errors
		/// this method must not block on SynchEvent
		/// </summary>
		/// <param name="e"></param>
		protected virtual void onError(AutomatonException e)
		{
			// implemented in you FSM
		}

		/// <summary>
		/// Called on dispose of StateMachine
		/// this method must not block on SynchEvent
		/// </summary>
		/// <param name="disposing">true if disposing by direct call to Dispose method</param>
		protected virtual void onDispose(bool disposing)
		{
			// implemented in you FSM
			// if(disposing)
			// {
			//		dispose your managed code
			// }
			// else
			// {
			//		dispose your unmanaged code
			// }
		}

		/// <summary>
		/// Implements state transitions on events
		/// </summary>
		/// <param name="ev"></param>
		public void castEvent(Event ev)
		{
			if (ev == null)
				throw new ArgumentNullException(Commons.GetArgName(new { ev }));

			lock (_syncEvent)
			{
#if DEBUG
				System.Diagnostics.Trace.WriteLine(
					String.Format("Lock acquired by '{0}' on thread {1}", Commons.GetCallerMethod(), System.Threading.Thread.CurrentThread.ManagedThreadId));
#endif

				// in FSM disposed throws exception
				throwIfDisposed();

				// notify new event arrived in the current state
				safeCallback(new Action<Event>(onEvent), ev);

				if (_edges[_currentState].ContainsKey(ev))
				{
					AI newState = _edges[_currentState][ev];
					AI oldState = _currentState;

					// notify state changing event
					var cancelEvent = new CancelEventArgs(false);
					safeCallback(new Action<AI, AI, CancelEventArgs>(onStateChanging), _currentState, newState, cancelEvent);

					if (!cancelEvent.Cancel)
					{
						// execute leave the previous state
						performStateExit(oldState);

						// execute enter the new state
						performStateEntry(newState);

						// actualize the new state
						_currentState = newState;

						// notify state changed event
						safeCallback(new Action<AI, AI>(onStateChanged), oldState, newState);
					}
				}
				else
				{
					var err = new AutomatonException("transition {0} not allowed in state {1}", ev, _currentState);
					if (!_ignoreInvalidTransitions)
						throw err;
					else
						safeCallbackError(err);
				}

				// Notify blocked thread that was transition completed
				System.Threading.Monitor.PulseAll(_syncEvent);
			}
		}

		private void performStateEntry(AI newState)
		{
			var entryState = newState as IState;
			if (entryState != null)
				safeCallback(new Action(entryState.onEntry)); // call state entry
		}

		private void performStateExit(AI oldState)
		{
			var exitState = oldState as IState;
			if (exitState != null)
				safeCallback(new Action(exitState.onExit)); // call state exit
		}

		protected void safeCallbackError(string methodName, Exception excpt)
		{
			safeCallbackError(new AutomatonException(string.Format("error executing callback: {0}", methodName), excpt));
		}

		protected void safeCallbackError(AutomatonException excpt)
		{
			Commons.SafeCallbackError(onError, excpt);
		}

		protected void safeCallback(Delegate action, params object[] args)
		{
			Commons.SafeCallback(action, (Exception e) => safeCallbackError(action.Method.Name, e), args);
		}

		#region IDispose implementation

		private int _disposeCount;

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected bool Disposed { get { return (_disposeCount > 0); } }

		protected void throwIfDisposed()
		{
			if (Disposed)
				throw new ObjectDisposedException(GetType().Name);
		}

		/// <summary>
		/// Dispose instance resources
		/// </summary>
		/// <param name="disposing">true if disposing by direct call</param>
		private void Dispose(bool disposing)
		{
			lock (_syncEvent)
			{
#if DEBUG
				System.Diagnostics.Trace.WriteLine(
					String.Format("Lock acquired by '{0}' on thread {1}", Commons.GetCallerMethod(), System.Threading.Thread.CurrentThread.ManagedThreadId));
#endif

				if (System.Threading.Interlocked.Increment(ref _disposeCount) == 1)
				{
					// disposal code here

					if (disposing)
					{
						safeCallback(new Action<bool>(onDispose), disposing);

						// Free other state (managed objects).
						foreach (var s in _edges.Keys)
						{
							var dispState = s as IDisposable;
							if (dispState != null)
								dispState.Dispose();
						}
						_edges.Clear();
						_currentState = default(AI);
					}

					// Free your own state (unmanaged objects).
					// Set large fields to null.
				}

				// Notify blocked thread that was transition completed
				System.Threading.Monitor.PulseAll(_syncEvent);
			}
		}

		// Use C# destructor syntax for finalization code.
		~AutomatonBase()
		{
			// Simply call Dispose(false).
			Dispose(false);
		}

		#endregion
	}
}