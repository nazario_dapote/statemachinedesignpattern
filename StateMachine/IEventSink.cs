﻿/********************************************************************************
Author		: Nazario D'Apote
E-Mail		: cons_nazario.dapote@gilbarco.com
License		: Copyright © Gilbarco S.r.l. 2016
*********************************************************************************/

namespace Gvr.StateMachine
{
	/// <summary>
	/// Event notification interface (IEventSink) — implemented by a context.
	/// This is the only way of interaction between the state classes and the context
	/// </summary>
	public interface IEventSink
	{
		/// <summary>
		/// execute state transaction
		/// </summary>
		/// <param name="e"></param>
		void castEvent(Event e);

		/// <summary>
		/// this locker should be used wherever a thread call castEvent
		/// </summary>
		object SynchEvent { get; }
	}
}